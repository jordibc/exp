# Exponential function on a scaled basis element of a geometric algebra

Combine the elements of `exp(i x)` for the cases `i^2 = -/+ 1`, which
lie on a circle and a hyperbola respectively.

The x position of the mouse (with respect the center point) is the `x`
in the formula.

The colors of the lines are arbitrary, just to show the different
parts/pieces of the sum.

You can zoom in/out with the mouse wheel too.


## Running for the first time

To get the react and material packages, you can just run this the
first time:

```sh
yarn
```

(Or `node install` too.) It will download the corresponding javascript
packages in a `node_modules` directory, as specified in the file
`package.json` like any other normal node program.

Then, you can create a release with:

```sh
npx shadow-cljs release app
```

The code to upload will be in the `public` directory (and you only
need `index.html` and `js/main.js`). You could upload this to a web
server (or use something like `python -m http.server -d public`) to
play with it.

After any changes to the code, you just need to run that last command
again.
