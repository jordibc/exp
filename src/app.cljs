(ns app
  (:require [reagent.core :as r]
            [reagent.dom :as rdom]
            [reagent-mui.components :as mui] ; see https://mui.com/material-ui
            [quil.core :as q]))              ; see http://quil.info/api


(def hyperbolic (r/atom false))
(def scale (r/atom 100))

(def cx (-> (. js/window -innerWidth) (/ 2)))
(def cy (-> (. js/window -innerHeight) (/ 2) (- 100)))


(defn circle [x y r]
  (let [d (* 2 r)]
    (q/ellipse x y d d)))


(defn from-pixel [[x y]]
  [(/ (- x cx) @scale) (/ (+ y cy) @scale)])


(defn to-pixel [[x y]]
  [(+ cx (* x @scale)) (- cy (* y @scale))])


(def colors [[155 0 0] [0 155 0] [0 0 155] [60 60 0] [0 60 60] [60 0 60]])


(defn draw []
  (q/background 255) ; clear screen

  ;; Reference circle.
  (q/stroke [0 0 255])
  (circle cx cy @scale)

  ;; All the segments.
  (q/stroke-weight 1)
  (let [[x _] (from-pixel [(q/mouse-x) 0])
        +- (if @hyperbolic + -)]
    (loop [term 1        ; current term in the taylor expansion of exp(x)
           [x0 y0] [0 0] ; current origin of the line
           i 0]          ; current depth level
      (let [[x1 y1] (case (mod i 4)
                      0 [(+ x0 term) y0]   ; ->  (moves right)
                      1 [x0 (+ y0 term)]   ; ^   (moves up)
                      2 [(+- x0 term) y0]  ; <-  (moves left or right)
                      3 [x0 (+- y0 term)]) ; v   (moves down or up)
            color (colors (mod i (count colors)))]
        (q/stroke color)
        (q/line (to-pixel [x0 y0]) (to-pixel [x1 y1]))
        (when (< i 50) ; 50 is an arbitrary number of terms
          (recur (-> term (* x) (/ (inc i))) ; next term in the taylor expansion
                 [x1 y1] ; we'll start from the last point
                 (inc i))))))) ; and go into the next depth level


(defn mouse-wheel [delta]
  (let [ds (if (< delta 0) 1.2 0.8)]
    (swap! scale #(* % ds)))
  (draw))


(defn main []
  (q/sketch
   :host "canvas"
   :mouse-moved draw ; instead of  :draw draw  (more efficient!)
   :mouse-wheel mouse-wheel
   :size [(. js/window -innerWidth) (- (. js/window -innerHeight) 200)])
  (rdom/render
   [:div
    [:h1 "Circular / Hyperbolic Geometry"]
    [:p "Combine the elements of " [:tt "exp(i x)"] " for the cases " [:tt "i^2 = -/+ 1"]
     " (which lie on a "
     [mui/button {:variant "outlined"  :on-click #(reset! hyperbolic false)} "circle"]
     " or a "
     [mui/button {:variant "contained" :on-click #(reset! hyperbolic true) } "hyperbola"]
     " respectively)."]]
   (. js/document getElementById "root")))
